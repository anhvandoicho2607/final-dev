angular.module('webphone')
  .controller('CallController', function($scope, $rootScope, $timeout , $interval) {
    var SIPDOMAIN = 'duy.tel4vn.com'
    var vm = this;
    var simple = null;
    var intervalTimer = null;
    var timer = 0;
    var registerUser = null;
    var audioPath = "sounds/";

    vm.callStatus = ''; // '': no call, 'ringing', 'connecting','connected'
    vm.duration = '--:--'
    vm.isMute = false;
    vm.isHold = false;

    function init(u) {
      registerUser = u;
      var options = {
        media: {
          local: {
            audio: document.getElementById('localAudio')
          },
          remote: {
            audio: document.getElementById('remoteAudio')
          }
        },
        ua: {
          uri: u.username + '@' + SIPDOMAIN,
          authorizationUser: u.username,
          password: u.password,
          wsServers: ['wss://vn01.tel4vn.com:7444'],
          stunServers: ['113.164.246.26:3478']
        }
      };
    
      simple = new SIP.Web.Simple(options);
      addSipEvents(simple);

    }

    function addSipEvents(simple){
      simple.on('new',function(){
        console.log('CALL : NEW')
        changeCallStatus('new');
      });

      simple.on('ringing',function($e){
        console.log('CALL : RINGING')
        changeCallStatus('ringing');
        vm.callInfo = {
          firstName : '',
          lastname : '',
          addr :'',
          number: $e.remoteIdentity.uri.user 
        }
        vm.isDialout = false;
      });

      simple.on('connecting',function(){
        console.log('CALL : CONNECTING')
        changeCallStatus('connecting');
      });

      simple.on('connected',function(){
        console.log('CALL : CONNECTED')
        changeCallStatus('connected');
        intervalTimer = $interval(function() {
          timer++;
          vm.duration = durationToTimer(timer);
        },1000);

      });

      simple.on('ended',function(){
        console.log('CALL : ENDED')
        changeCallStatus('');
        timer = 0;
        vm.duration = durationToTimer(timer);
        $interval.cancel(intervalTimer);
        vm.isDialout = true;
      });

      simple.on('registered', function() {
        console.log('REGISTERED');
        $rootScope.$broadcast('REGISTERSTATUS', {isRegister: true, username: registerUser.username})
      })
      simple.on('unregistered', function() {
        console.log('UNREGISTERED');
        $rootScope.$broadcast('REGISTERSTATUS', {isRegister: false, username: registerUser.username})
      })

    }

    function changeCallStatus(status){
      $timeout(function(){
        vm.callStatus = status;
      },5);
    }

    function durationToTimer(duration){
      var hours = Math.floor(duration / 3600);
      var minutes = Math.floor((duration - (hours*3600))/60);
      var seconds = Math.floor(duration -(hours*3600) -(minutes*60));
      if(hours<10) {
        hours = "0" + hours;
      }
      if(minutes < 10) {
        minutes = "0" + minutes;
      }
      if(seconds < 10) {
        seconds = "0" + seconds;
      }
      if(hours==="00") {
        hours = '';
      }else {
        hours += ":"
      }

      return hours + minutes + ":" + seconds
    }

    $rootScope.$on('CALL', function(e, data) {
      console.log('CALL:', data);
      vm.callInfo = data;
      simple.call(data.number + '@' + SIPDOMAIN);
    });

    $rootScope.$on('REGISTER', function(e, data) {
      console.log('REGSITER:', data);
      init(data);
    });

    $rootScope.$on('UNREGISTER', function(e, data) {
      console.log('UNREGSITER:', data);
      simple.ua.unregister();
    });

    vm.hangup = function() {
      simple.hangup();
    }

    vm.accept = function(){
      simple.answer();
    }

    vm.reject = function(){
      simple.reject();
    }

    vm.toggleHold = function(){
      if(vm.isHold){
        simple.unhold();
      }else {
        simple.hold();
      }
      vm.isHold = !vm.isHold;
    }

    vm.toggleMute = function(){
      if(vm.isMute){
        simple.unmute();
      }else {
        simple.mute();
      }
      vm.isMute = !vm.isMute;
    }

    vm.dtmf = function(n) {
      simple.sendDTMF(n);
      //play dtmf
      playDtmfSound(n);
    }
    function playDtmfSound(n) {
      if(n==='*') {
        n = 'star'; 
      }else if(n==='#') {
        n = 'hash';
      }
      var path =  audioPath + 'dtmf/dtmf-' + n + '.mp3'; 
      if(window.navigator.platform.indexOf('Win')>=0) {
        path = path.replace(/\//g, '\\');
      }
      vm.audioSrc = path;

      $timeout(function() {
        vm.audioSrc = '';
      },500);
    }
});

