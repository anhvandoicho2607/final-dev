angular.module('webphone')
  .controller('ContactController', function($scope, $rootScope, $timeout) {
    var vm = this;

    var defaultContacts = [
        {firstName:'Nguyễn', lastName:'Duy',addr:'Bình Thạnh' , number: '7117'},
        {firstName:'Trần', lastName:'Long',addr:'Q2', number: '7118'},
        {firstName:'Nguyễn', lastName:'Tân',addr:'Bình Thạnh', number: '7119'},
        {firstName:'Lê', lastName:'Khánh',addr:'Q7', number: '7120'},
      ];

      vm.contacts = [];

      vm.isRegistered = false;
      vm.showRegisterModal = false;
      vm.showAddContact = false;

      $timeout(function() {
        init();
      },200);
      
      function init() {
        var r, c;
        try {
          r = JSON.parse(localStorage.getItem('r'));
        }catch(e) {
          r = null;
        }
        if(r) {
          $rootScope.$broadcast('REGISTER', r);
        }
        try {
          c = JSON.parse(localStorage.getItem('c'));
        }catch(e) {
          c = null;
        }
        if(!c) {
          c = defaultContacts;
        }      
        vm.contacts = c;  
      }

      vm.call = function(c) {
        console.log(c);
        $rootScope.$broadcast('CALL', c);
      }

      vm.unregister = function(){
        console.log ('unregister');
        $rootScope.$broadcast('UNREGISTER',{})
      }

      vm.register  = function(u) {
        console.log('--->',u);
        // TODO: process register
        if(u.username == ''  || u.username == null || u.username == undefined || u.password == ''  || u.password == null|| u.password == undefined ){
          confirm("Thông tin nhập không được trống");
          return "";
          
        }else{
            $rootScope.$broadcast('REGISTER', u);
            vm.showRegisterModal=false
            // save register information
            localStorage.setItem('r',JSON.stringify(u));
        }
      }

      vm.delete = function(c){
        if(confirm("Are you sure to delete "+c.number)) {
            _.remove(vm.contacts, function(item) {
              return item.number === c.number;
            });
            // save contact list
            localStorage.setItem('c', JSON.stringify(vm.contacts));
          }
      }

      vm.save = function(c) {
        // console.log(c);
        if(c.firstName == ''  || c.firstName == null|| c.firstName == undefined || c.lastName == ''  || c.lastName == null|| c.lastName == undefined || c.addr == ''  || c.addr == null  || c.addr == undefined || c.number == ''  || c.number == null || c.number == undefined ){
          confirm("Thông tin nhập không được trống");
          return "";
          
        }else{
          vm.contacts.push({
              firstName: c.firstName,
              lastName: c.lastName,
              addr: c.addr,
              number: c.number
            });
            c.firstName = '';
            c.lastName = '';
            c.addr = '';
            c.number = '';

            vm.showAddContact = false;
            // save contact list
            localStorage.setItem('c', JSON.stringify(vm.contacts));
        }
        
      }

      vm.callAlt = function(text) {
        var c = _.find(vm.contacts, function(o) { 
          return o.number == text; 
        });
        
        if(!c) {
          c = {
            firstName:'',
            lastName:'',
            number: text 
          }  
        }  
        vm.call(c);
      }

      vm.callByEnter = function(e, text) {
        if(!vm.isRegistered) {
          return;
        }
        if(e.keyCode === 13) {
          vm.callAlt(text); 
        }
      }
      vm.saveByEnter = function(e, text) {
        if(!vm.isRegistered) {
          return;
        }
        if(e.keyCode === 13) {
          vm.save(text); 
        }
      }
      vm.registerByEnter = function(e, text) {
        if(vm.isRegistered) {
          return;
        }
        if(e.keyCode === 13) {
          vm.register(text);
        }
      }

      $rootScope.$on('REGISTERSTATUS', function(e, data) {
        console.log('REGISTERSTATUS:',data);
        $timeout(function() {
          vm.isRegistered = data.isRegister;
          vm.registerUser = data.username;
        },5);
      });  

})
